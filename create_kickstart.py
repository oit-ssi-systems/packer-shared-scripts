#!/usr/bin/env python
from __future__ import print_function
from jinja2 import Environment, FileSystemLoader
import os
import sys

template_vars = {
    'use_dvd': os.environ.get("TEMPLATE_USE_DVD", False),
    'build_os': os.environ.get("BUILD_OS"),
    'template_vm_hostname': os.environ.get("TEMPLATE_VM_HOSTNAME", "packer-template.oit.duke.edu"),
    'template_password_crypt': os.environ.get("TEMPLATE_PASSWORD_CRYPT"),
    'template_ssh_pubkey': os.environ.get("TEMPLATE_SSH_PUBKEY"),
}


def setup_j2():
    j2_env = Environment(loader=FileSystemLoader('ks'),
                         trim_blocks=True)
    template = j2_env.get_template('ks.cfg.j2')
    return template


def render_kickstart(template, template_vars):
    output = template.render(template_vars)
    return output


def main():
    if not os.environ.get("TEMPLATE_PASSWORD_CRYPT"):
        print('No crypted password set in ${TEMPLATE_PASSWORD_CRYPT}', file=sys.stderr)
        return 1
    else:
        template = setup_j2()
        ks_output = render_kickstart(template, template_vars)
        print(ks_output)


if __name__ == "__main__":
    sys.exit(main())
