#!/bin/bash

# Delete old kernels?

# Clean package cache
echo "Cleaning up extra files"

# Zero out the rest of the free space using dd, then delete the written file.
echo "Reclaming free space on disk"
# dd if=/dev/zero of=/EMPTY bs=1M
# rm -f /EMPTY
fstrim -av

# Add `sync` so Packer doesn't quit too early, before the large file is deleted.
sync
