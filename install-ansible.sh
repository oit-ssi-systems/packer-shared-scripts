#!/bin/bash
set -euxo pipefail

if [[ ${BUILD_OS} == "rhel8" || ${BUILD_OS} == "centos-8" || ${BUILD_OS} == "centos-stream-8" || ${BUILD_OS} == "centos-stream-9" ]]; then
    # List available repos for posterity
    yum repolist
    # Install Ansible
    yum -y --nogpgcheck --disableplugin=fastestmirror install ansible-core
elif [[ ${BUILD_OS} == "rhel7.5-EUS" ]]; then
    exit 0;
fi
