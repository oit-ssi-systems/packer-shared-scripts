#!/bin/bash

if [[ ${#} -ne 1 ]]; then
    echo 'Pass 1 parameter equal to the type of os\n' \
         ' you want to clean up. e.g. ubuntu, centos.'
    exit 1
fi

if [[ -z ${VAULT_TOKEN} ]]; then
	echo "No VAULT_TOKEN set in environment"
	exit 3
fi

export GOVC_USERNAME=$(vault kv get -field=username ssi-systems/ad/creds/vcenter)
export GOVC_PASSWORD=$(vault kv get -field=current_password ssi-systems/ad/creds/vcenter)

set -euxo pipefail

export GOVC_URL=${GOVC_URL-"vmware-test.oit.duke.edu"}
export GOVC_DATACENTER=${GOVC_DATACENTER-"Main Datacenter"}
export GOVC_CLUSTER=${GOVC_CLUSTER-"ATC Lab"}

govc find / -type m -name "packer-${1}*"

govc find / -type m -name "packer-${1}*" | xargs -I VMNAME govc vm.power -off 'VMNAME'
govc find / -type m -name "packer-${1}*" | xargs -I VMNAME govc vm.destroy 'VMNAME'