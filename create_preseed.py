#!/usr/bin/env python
from __future__ import print_function
from jinja2 import Environment, FileSystemLoader
import os
import sys

template_vars = {
    'template_vm_hostname': os.environ.get("TEMPLATE_VM_HOSTNAME", "ubuntu-packer-template.oit.duke.edu"),
    'template_password_crypt': os.environ.get("TEMPLATE_PASSWORD_CRYPT"),
    'template_root_password_crypt': os.environ.get("TEMPLATE_ROOT_PASSWORD_CRYPT"),
}


def setup_j2():
    j2_env = Environment(loader=FileSystemLoader('preseed'),
                         trim_blocks=True)
    template = j2_env.get_template('preseed.cfg.j2')
    return template


def render_preseed(template, template_vars):
    output = template.render(template_vars)
    return output


def main():
    if not os.environ.get("TEMPLATE_PASSWORD_CRYPT"):
        print('No crypted password set in ${TEMPLATE_PASSWORD_CRYPT}', file=sys.stderr)
        return 1
    elif not os.environ.get("TEMPLATE_ROOT_PASSWORD_CRYPT"):
        print('No crypted root password set in ${TEMPLATE_ROOT_PASSWORD_CRYPT}', file=sys.stderr)
        return 1
    else:
        template = setup_j2()
        preseed_output = render_preseed(template, template_vars)
        print(preseed_output)


if __name__ == "__main__":
    sys.exit(main())
