#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

if [[ ! -z ${TEMPLATE_USERNAME} ]]; then
    echo "${TEMPLATE_USERNAME} ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/provisioning && \
    echo "${TEMPLATE_USERNAME} configured in sudoers.d/provisioning"
else
    echo "No TEMPLATE_USERNAME set in environment"
    exit 3
fi

sudo apt-get update
sudo apt-get install -y ansible
