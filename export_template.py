#!/usr/bin/env python
'''either export templates into a content library or deploy them!'''

import sys
import os
import argparse
import requests
import urllib3
from urllib3.exceptions import InsecureRequestWarning

urllib3.disable_warnings(InsecureRequestWarning)


def parse_args():
    '''Parse the args'''
    parser = argparse.ArgumentParser(description='get stuff from vsphere rest api')
    parser.add_argument('--username',
                        action="store",
                        default=os.environ.get('VCAPI_USERNAME', None))
    parser.add_argument('--password',
                        action="store",
                        default=os.environ.get('VCAPI_PASSWORD', None))
    parser.add_argument('--vc',
                        action="store", help='vcenter hostname',
                        default=os.environ.get('VCAPI_HOSTNAME', None))
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--vm',
                       action="store", help='vm to operate on')
    group.add_argument('--deploy',
                       action="store", help='Deploy templates')
    parser.add_argument('--cl',
                        action="store", help='Content Library to write to',
                        default=os.environ.get('VCAPI_CONTENT_LIBRARY', None),
                        required=True)
    parser.add_argument('--cli',
                        action="store", help='Content Library item name',
                        default=os.environ.get('VCAPI_CONTENT_LIBRARY_ITEM', None))

    return parser.parse_args()


def open_session(args):
    '''Create authenticated session to target vcenter'''
    session = requests.Session()
    session.verify = False
    res = session.post('https://'+args.vc+'/rest/com/vmware/cis/session',
                       auth=(args.username, args.password))
    if res.status_code == 200:
        return session
    print("Error establishing vcenter session: %s", res)
    return False


def get_vmid(session, args):
    '''Retrieve vm id corresponding to source vm'''
    res = session.get('https://'+args.vc+'/rest/vcenter/vm?filter.names='+args.vm)
    if res.status_code == 200:
        resp = res.json()
        if resp.get("value", None) is not None:
            vmid = resp.get("value", None)[0].get("vm", None)
            return vmid
        print("No value in response? %s", resp)
        return None
    return None


def get_content_library_id(session, vcenter, name):
    '''Retrieve content library ID corresponding to target content library'''
    res = session.post(
        'https://'+vcenter+'/rest/com/vmware/content/library?~action=find',
        json={
            "spec": {
                "name": name,
                "type": "LOCAL"
                }
            }
        )
    if res.status_code == 200:
        clid = res.json().get("value")[0]
        return clid
    return None


def get_content_library_item_id(session, vcenter, clid, name):
    '''Retrieve content library *item* id corresponding to target content library item name'''
    res = session.post(
        'https://'+vcenter+'/rest/com/vmware/content/library/item?~action=find',
        json={
            "spec": {
                "name": name,
                "library_id": clid
                }
            }
        )
    if res.status_code == 200:
        if res.json().get("value") == []:
            return None
    return res.json().get("value")[0]


def copy_library_item(session, vcenter, content_library_id, source_item_id, destination):
    '''Copy/Clone a library item'''
    res = session.post(
        'https://'+vcenter+'/rest/com/vmware/content/library/item/id:' +
        source_item_id+'?~action=copy',
        json={
            "destination_create_spec": {
                "library_id": content_library_id,
                "name": destination
                }
            }
        )
    if res.status_code == 200:
        if res.json().get("value"):
            update_library_item_description(session, vcenter, res.json().get("value"))
            return True

        return None
    else:
        return False


def convert_vm_to_content_item(session, args, clid, vmid):
    ''' creates a new content library item with new content! '''
    res = session.post(
        'https://'+args.vc+'/rest/com/vmware/vcenter/ovf/library-item',
        json={
            "target": {
                "library_id": clid
                },
            "source": {
                "type": "VirtualMachine",
                "id": vmid
                },
            "create_spec": {
                "description": "incoming template build: " + os.environ.get('BUILD_HASH_SHORT')
                               + "\n"
                               + " source_branch: " + os.environ.get('CI_COMMIT_REF', "unset")
                               + "\n"
                               + " source_job: " + os.environ.get('CI_JOB_URL', "unset")
                               + "\n"
                               + " source_pipe: " + os.environ.get('CI_PIPELINE_URL', "unset")
                               + "\n"
                               + "source_vm: "+args.vm,
                "name": args.cli
                }
            }
        )
    if res.status_code == 200:
        if res.json().get("value").get("succeeded", None):
            return True
        return None
    return None


def update_content_library_item(session, args, clid, vmid, cl_item_id):
    ''' updates an existing content library item with new content '''
    res = session.post(
        'https://'+args.vc+'/rest/com/vmware/vcenter/ovf/library-item',
        json={
            "target": {
                "library_id": clid,
                "library_item_id": cl_item_id
                },
            "source": {
                "type": "VirtualMachine",
                "id": vmid
                },
            "create_spec": {
                "description": "incoming template build: " + os.environ.get('BUILD_HASH_SHORT')
                               + "\n"
                               + " source_branch: " + os.environ.get('CI_COMMIT_REF', "unset")
                               + "\n"
                               + " source_job: " + os.environ.get('CI_JOB_URL', "unset")
                               + "\n"
                               + " source_pipe: " + os.environ.get('CI_PIPELINE_URL', "unset")
                               + "\n"
                               + " source_vm: "+args.vm,
                "name": args.cli
                }
            }
        )
    if res.status_code == 200:
        if res.json().get("value").get("succeeded", None):
            return True
        return None
    return None


def add_blank_floppy(session, args, vmid):
    '''Adds blank floppy to target vm'''
    res = session.post(
        'https://'+args.vc+'/rest/vcenter/vm/'+vmid+'/hardware/floppy',
        json={
            "spec": {
                }
            }
        )
    if res.status_code == 200:
        floppy_id = res.json().get("value")
        print("added new floppy drive: %s", floppy_id)
        return floppy_id
    return None


def update_library_item_description(session, vcenter, cl_item_id):
    '''Updates the description of a content library item with current build info'''
    res = session.patch(
        'https://'+vcenter+'/rest/com/vmware/content/library/item/id:'+cl_item_id,
        json={
            "update_spec": {
                "description": "incoming template for build: "
                               + os.environ.get('BUILD_HASH_SHORT', "Build hash unset!")
                               + "\n"
                               + " source_branch: " + os.environ.get('CI_COMMIT_REF', "unset")
                               + " source_job: " + os.environ.get('CI_JOB_URL', "unset")
                               + " source_pipe: " + os.environ.get('CI_PIPELINE_URL', "unset")}
            }
        )
    if res.status_code == 200:
        print("Updated Content Library Item description")
        return True
    print("Failed to update Content Library Item description")
    return None


def update_library_item(session, args, cl_item_id, changes):
    '''

    updates the content library item with arbitrary properties.
    pass in a changes hash with properties you want to update

    '''
    res = session.patch('https://'+args.vc+'/rest/com/vmware/content/library/item/id:'+cl_item_id,
                        json={"update_spec": changes})
    if res.status_code == 200:
        print("Updated Content Library item properties")
        print(changes)
        return True
    return None


def delete_vm(session, args, vmid):
    keep_vm = os.environ.get('KEEP_SOURCE_VM', False)
    if (keep_vm == "True") or (keep_vm is True):
        print("Not deleting vm: %s", args.vm)
        return None
    else:
        '''Deletes the source VM'''
        res = session.delete('https://'+args.vc+'/rest/vcenter/vm/'+vmid)
        if res.status_code == 200:
            print("VM {} deleted.".format(args.vm))
            return True
        return None


def delete_content_library_item(session, args, cl_item_id):
    ''' Deletes a content library item '''
    res = session.delete('https://'+args.vc+'/rest/com/vmware/content/library/item/id:'+cl_item_id)
    if res.status_code == 200:
        print("Content Library Item Id: {} deleted".format(cl_item_id))
        return True
    return None


def rename_content_library_item(session, args, cl_item_id, name):
    ''' renames a library item by ID '''
    changes = {"name": name}
    if update_library_item(session, args, cl_item_id, changes):
        print("Content Library Item {} renamed to {}".format(args.cli, name))
        return True
    return None


def deploy_template(session, args, clid, cl_item_id):
    '''

    Deploys templates from a git branch version (e.g. os-version-development)
      to the production template name (e.g. os-version)

    '''
    existing_cl_item_id = get_content_library_item_id(session, args.vc, clid, args.deploy)
    existing_previous_cl_item_id = get_content_library_item_id(session, args.vc, clid, args.deploy + '.previous')

    print("deploy_template: {} to {}".format(args.cli, args.deploy))
    if existing_cl_item_id is not None:
        # remove old .previous content library item
        if existing_previous_cl_item_id is not None:
            print("deploy_template: old {} removed".format(args.deploy + '.previous'))
            delete_previous = delete_content_library_item(session, args, existing_previous_cl_item_id)
            return delete_previous
        rename_previous = rename_content_library_item(session, args, existing_cl_item_id, args.deploy + '.previous')
        if rename_previous is not None:
            print("deploy_template: .previous renamed; deploying {} to {}".format(args.cli, args.deploy))
            rename_target = rename_content_library_item(session, args, cl_item_id, args.deploy)
            return rename_target
    else:
        return copy_library_item(session, args.vc, clid, cl_item_id, args.deploy)
    return None


def main():
    '''

    This is the main method

    '''
    args = parse_args()
    session = open_session(args)
    cl_id = get_content_library_id(session, args.vc, args.cl)
    cl_item_id = get_content_library_item_id(session, args.vc, cl_id, name=args.cli)

    if args.deploy:
        deploy_template(session, args, cl_id, cl_item_id)
        sys.exit()

    vmid = get_vmid(session, args)
    if cl_item_id is not None:
        print("Updating content library item: {}".format(args.cli))
        conversion = update_content_library_item(session, args, cl_id, vmid, cl_item_id)
        update_library_item_description(session, args.vc, cl_item_id)
    else:
        print("Creating new content library item.")
        conversion = convert_vm_to_content_item(session, args, cl_id, vmid)
    if conversion is not None:
        delete_vm(session, args, vmid)
        print("Conversion succesful! deleting: {}".format(args.vm))
    else:
        sys.exit(1)


if __name__ == "__main__":
    sys.exit(main())
