#!/bin/bash

if [[ ${#} -ne 1 ]]; then
    echo 'Pass 1 parameter equal to ${something}.json\n' \
         ' where something is a packer json filename.'
    exit 1
fi

if [[ -z ${BUILD_OS} ]]; then
    echo "No BUILD_OS set, exiting"
    exit 3
fi

if [[ -n ${VAULT_TOKEN} ]]; then
    mkdir ~/.ssh;
    chmod 0700 ~/.ssh;
    vault kv get -field=key ssi-systems/kv/linux/template-build/packer-build-ssh > ~/.ssh/id_ed25519
    vault kv get -field=pub ssi-systems/kv/linux/template-build/packer-build-ssh > ~/.ssh/id_ed25519.pub
    chmod 600 ~/.ssh/id_ed25519
else
    echo "No VAULT_TOKEN set in environment"
    exit 3
fi

export TEMPLATE_PASSWORD=$(vault kv get -field=build-password ssi-systems/kv/linux/template-build/packer-${BUILD_OS})

if [[ -z ${TEMPLATE_PASSWORD} ]]; then
    echo "No template password returned"
    exit 1
fi

export GOVC_USERNAME=$(vault kv get -field=username ssi-systems/ad/creds/vcenter)
export GOVC_PASSWORD=$(vault kv get -field=current_password ssi-systems/ad/creds/vcenter)

if [[ -z ${GOVC_USERNAME} ]]; then
    echo "No vSphere username";
    exit 2;
elif [[ -z ${GOVC_PASSWORD} ]]; then
    echo "No vSphere password";
    exit 2;
fi

set -euxo pipefail

export GOVC_URL=${GOVC_URL-"vmware-test.oit.duke.edu"}
export GOVC_RESOURCE_POOL=${GOVC_RESOURCE_POOL-"Copper"}
export GOVC_FOLDER=${GOVC_FOLDER-"OIT-SSI/Windows/PackerImages"}
export GOVC_NETWORK=${GOVC_NETWORK-"OIT-DC-ATC-LINUX-BUILD-01"}
export GOVC_DATACENTER=${GOVC_DATACENTER-"Main Datacenter"}
export GOVC_DATASTORE=${GOVC_DATASTORE-"UNI-ATC-01-Platinum-L26"}
export GOVC_CLUSTER=${GOVC_CLUSTER-"ATC Lab"}

export BUILD_HASH=${CI_COMMIT_SHA-$(git rev-parse HEAD)}
export BUILD_HASH_SHORT=${CI_COMMIT_SHORT_SHA-$(git rev-parse --short=8 HEAD)}
export BUILD_OUTPUT=${BUILD_OUTPUT-packer-output}
export PACKER_ROOT_DIR=${PACKER_ROOT_DIR-${PWD}}
export PACKER_LOG=${PACKER_LOG-"0"}
export TEMPLATE_USERNAME="packer"

mkdir -p ${BUILD_OUTPUT}

if [ -f "${BUILD_OS}.pkrvars.hcl" ]; then
    packer init -upgrade -var-file=${BUILD_OS}.pkrvars.hcl ${1}.pkr.hcl
    packer build ${PACKER_BUILD_ARGS-} -var-file=${BUILD_OS}.pkrvars.hcl ${1}.pkr.hcl
else
    packer build ${PACKER_BUILD_ARGS-} -var-file="${BUILD_OS}.json" ${1}.json
fi
