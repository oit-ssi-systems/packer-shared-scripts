#!/bin/bash

if [[ -z ${TEMPLATE_INFO_FILE} ]]; then
    echo "Please pass a file where you want info put."
    exit 1
fi

if [[ ! -d $(dirname ${TEMPLATE_INFO_FILE}) ]]; then
    echo "creating directory $(dirname ${TEMPLATE_INFO_FILE}) for template-info.txt";
    mkdir -p $(dirname ${TEMPLATE_INFO_FILE});
fi

set -euxo pipefail

echo "BUILD_HASH_SHORT=${BUILD_HASH_SHORT}" >> ${TEMPLATE_INFO_FILE}
echo "BUILD_OS=${BUILD_OS}" >> ${TEMPLATE_INFO_FILE}
