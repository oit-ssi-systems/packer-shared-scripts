#!/bin/bash

TEMPLATE_PASSWORD=$(pwgen -ncBv 16 1)
TEMPLATE_PASSWORD_CRYPT=$(openssl passwd -6 "${TEMPLATE_PASSWORD}")
export TEMPLATE_PASSWORD
export TEMPLATE_PASSWORD_CRYPT

TEMPLATE_ROOT_PASSWORD=$(pwgen -ncBv 24 1)
TEMPLATE_ROOT_PASSWORD_CRYPT=$(openssl passwd -6 "${TEMPLATE_ROOT_PASSWORD}")
export TEMPLATE_ROOT_PASSWORD
export TEMPLATE_ROOT_PASSWORD_CRYPT

if [[ -z ${BUILD_OS} ]]; then
    echo "No BUILD_OS set, exiting"
    exit 3
fi

if [[ -n ${VAULT_TOKEN} ]]; then
    vault kv put "ssi-systems/kv/linux/template-build/packer-${BUILD_OS}" \
        build-password="${TEMPLATE_PASSWORD}" \
        build-password-crypt="${TEMPLATE_PASSWORD_CRYPT}" \
        build-root-password="${TEMPLATE_ROOT_PASSWORD}" \
        build-root-password-crypt="${TEMPLATE_ROOT_PASSWORD_CRYPT}" && \
        echo "Stored secrets in ssi-systems/kv/linux/template-build/packer-${BUILD_OS}"
else
    echo "No VAULT_TOKEN set in environment"
    exit 3
fi
